﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace ItemMod
{
    public partial class Form1 : Form
    {
        public string sqlDataSource;
        SQLiteConnection sqlConnect = new SQLiteConnection();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddEleProperty_Click(object sender, EventArgs e)
        {
            listProperties.Items.Add(comboElement.Text + " : " + txtEleValue.Text);
        }

        private void checkRoundsEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if(checkRoundsEnabled.Checked == true)
            {
                txtRounds.Enabled = true;
            }
            else
            {
                txtRounds.Enabled = false;
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            var temp = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            sqlDataSource = "Data Source=" + temp + "\\itemdb.store";
            var sqlCommand = new SQLiteCommand();
            sqlCommand = sqlConnect.CreateCommand();

        }
    }
}
