﻿namespace ItemMod
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkRoundsEnabled = new System.Windows.Forms.CheckBox();
            this.txtRounds = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtEleValue = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboElement = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnRemoveEleProperty = new System.Windows.Forms.Button();
            this.btnAddEleProperty = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.listProperties = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkSetItem = new System.Windows.Forms.CheckBox();
            this.cmbItemSuffix = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbItemPrefix = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbItemQuality = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbItemType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSPD = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAGI = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDEX = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtINT = new System.Windows.Forms.TextBox();
            this.txtSTR = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMDEF = new System.Windows.Forms.TextBox();
            this.txtPDEF = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMATK = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPATK = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1381, 609);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.btnAddItem);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1373, 576);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Items";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkRoundsEnabled);
            this.groupBox3.Controls.Add(this.txtRounds);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.txtEleValue);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.comboElement);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.btnRemoveEleProperty);
            this.groupBox3.Controls.Add(this.btnAddEleProperty);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.listProperties);
            this.groupBox3.Location = new System.Drawing.Point(628, 136);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(737, 377);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Properties";
            // 
            // checkRoundsEnabled
            // 
            this.checkRoundsEnabled.AutoSize = true;
            this.checkRoundsEnabled.Location = new System.Drawing.Point(162, 100);
            this.checkRoundsEnabled.Name = "checkRoundsEnabled";
            this.checkRoundsEnabled.Size = new System.Drawing.Size(22, 21);
            this.checkRoundsEnabled.TabIndex = 17;
            this.checkRoundsEnabled.UseVisualStyleBackColor = true;
            this.checkRoundsEnabled.CheckedChanged += new System.EventHandler(this.checkRoundsEnabled_CheckedChanged);
            // 
            // txtRounds
            // 
            this.txtRounds.Enabled = false;
            this.txtRounds.Location = new System.Drawing.Point(90, 96);
            this.txtRounds.Name = "txtRounds";
            this.txtRounds.Size = new System.Drawing.Size(66, 26);
            this.txtRounds.TabIndex = 16;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 20);
            this.label18.TabIndex = 15;
            this.label18.Text = "Rounds";
            // 
            // txtEleValue
            // 
            this.txtEleValue.Location = new System.Drawing.Point(365, 38);
            this.txtEleValue.Name = "txtEleValue";
            this.txtEleValue.Size = new System.Drawing.Size(66, 26);
            this.txtEleValue.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(309, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 20);
            this.label17.TabIndex = 14;
            this.label17.Text = "Value";
            // 
            // comboElement
            // 
            this.comboElement.FormattingEnabled = true;
            this.comboElement.Items.AddRange(new object[] {
            "Wind",
            "Water",
            "Earth",
            "Fire",
            "Shadow",
            "Holy",
            "Poison"});
            this.comboElement.Location = new System.Drawing.Point(93, 38);
            this.comboElement.Name = "comboElement";
            this.comboElement.Size = new System.Drawing.Size(197, 28);
            this.comboElement.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 20);
            this.label16.TabIndex = 12;
            this.label16.Text = "Element";
            // 
            // btnRemoveEleProperty
            // 
            this.btnRemoveEleProperty.Location = new System.Drawing.Point(689, 311);
            this.btnRemoveEleProperty.Name = "btnRemoveEleProperty";
            this.btnRemoveEleProperty.Size = new System.Drawing.Size(42, 33);
            this.btnRemoveEleProperty.TabIndex = 11;
            this.btnRemoveEleProperty.Text = "-";
            this.btnRemoveEleProperty.UseVisualStyleBackColor = true;
            // 
            // btnAddEleProperty
            // 
            this.btnAddEleProperty.Location = new System.Drawing.Point(505, 311);
            this.btnAddEleProperty.Name = "btnAddEleProperty";
            this.btnAddEleProperty.Size = new System.Drawing.Size(42, 33);
            this.btnAddEleProperty.TabIndex = 10;
            this.btnAddEleProperty.Text = "+";
            this.btnAddEleProperty.UseVisualStyleBackColor = true;
            this.btnAddEleProperty.Click += new System.EventHandler(this.btnAddEleProperty_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(501, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 20);
            this.label15.TabIndex = 9;
            this.label15.Text = "Properties list";
            // 
            // listProperties
            // 
            this.listProperties.FormattingEnabled = true;
            this.listProperties.ItemHeight = 20;
            this.listProperties.Location = new System.Drawing.Point(505, 41);
            this.listProperties.Name = "listProperties";
            this.listProperties.Size = new System.Drawing.Size(226, 264);
            this.listProperties.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkSetItem);
            this.groupBox2.Controls.Add(this.cmbItemSuffix);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbItemPrefix);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbItemQuality);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbItemType);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(616, 507);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "General";
            // 
            // checkSetItem
            // 
            this.checkSetItem.AutoSize = true;
            this.checkSetItem.Location = new System.Drawing.Point(10, 342);
            this.checkSetItem.Name = "checkSetItem";
            this.checkSetItem.Size = new System.Drawing.Size(96, 24);
            this.checkSetItem.TabIndex = 10;
            this.checkSetItem.Text = "Set Item";
            this.checkSetItem.UseVisualStyleBackColor = true;
            // 
            // cmbItemSuffix
            // 
            this.cmbItemSuffix.FormattingEnabled = true;
            this.cmbItemSuffix.Items.AddRange(new object[] {
            "*THIS IS NOT IMPLEMENTED*",
            "*NEED TABLE OF SUFFIX STATS*",
            "of the gloom stalker",
            "of the fire lord",
            "of the glacial court",
            "of the luminous"});
            this.cmbItemSuffix.Location = new System.Drawing.Point(114, 257);
            this.cmbItemSuffix.Name = "cmbItemSuffix";
            this.cmbItemSuffix.Size = new System.Drawing.Size(272, 28);
            this.cmbItemSuffix.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Item suffix";
            // 
            // cmbItemPrefix
            // 
            this.cmbItemPrefix.FormattingEnabled = true;
            this.cmbItemPrefix.Items.AddRange(new object[] {
            "*THIS IS NOT IMPLEMENTED*",
            "*NEEDS STATS PER PREFIX DECIDED ON*",
            "Flame weaver\'s",
            "Conjurer\'s",
            "Aquamancer\'s"});
            this.cmbItemPrefix.Location = new System.Drawing.Point(114, 202);
            this.cmbItemPrefix.Name = "cmbItemPrefix";
            this.cmbItemPrefix.Size = new System.Drawing.Size(272, 28);
            this.cmbItemPrefix.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Item prefix";
            // 
            // cmbItemQuality
            // 
            this.cmbItemQuality.FormattingEnabled = true;
            this.cmbItemQuality.Items.AddRange(new object[] {
            "Common",
            "Rare",
            "Epic",
            "Soul Forged",
            "Unique",
            "Legendary"});
            this.cmbItemQuality.Location = new System.Drawing.Point(114, 148);
            this.cmbItemQuality.Name = "cmbItemQuality";
            this.cmbItemQuality.Size = new System.Drawing.Size(272, 28);
            this.cmbItemQuality.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Item quality";
            // 
            // cmbItemType
            // 
            this.cmbItemType.FormattingEnabled = true;
            this.cmbItemType.Items.AddRange(new object[] {
            "Consumable",
            "Weapon",
            "Armor",
            "Accessory",
            "Pet Accessory",
            "Quest"});
            this.cmbItemType.Location = new System.Drawing.Point(114, 95);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Size = new System.Drawing.Size(272, 28);
            this.cmbItemType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Item type";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(114, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(272, 26);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item name";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSPD);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtAGI);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtDEX);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtINT);
            this.groupBox1.Controls.Add(this.txtSTR);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtMDEF);
            this.groupBox1.Controls.Add(this.txtPDEF);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtMATK);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtPATK);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(628, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(737, 123);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Main Stats";
            // 
            // txtSPD
            // 
            this.txtSPD.Location = new System.Drawing.Point(652, 59);
            this.txtSPD.Name = "txtSPD";
            this.txtSPD.Size = new System.Drawing.Size(66, 26);
            this.txtSPD.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(604, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 20);
            this.label14.TabIndex = 13;
            this.label14.Text = "SPD";
            // 
            // txtAGI
            // 
            this.txtAGI.Location = new System.Drawing.Point(505, 78);
            this.txtAGI.Name = "txtAGI";
            this.txtAGI.Size = new System.Drawing.Size(66, 26);
            this.txtAGI.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(456, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "AGI";
            // 
            // txtDEX
            // 
            this.txtDEX.Location = new System.Drawing.Point(505, 46);
            this.txtDEX.Name = "txtDEX";
            this.txtDEX.Size = new System.Drawing.Size(66, 26);
            this.txtDEX.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(456, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "DEX";
            // 
            // txtINT
            // 
            this.txtINT.Location = new System.Drawing.Point(365, 78);
            this.txtINT.Name = "txtINT";
            this.txtINT.Size = new System.Drawing.Size(66, 26);
            this.txtINT.TabIndex = 10;
            // 
            // txtSTR
            // 
            this.txtSTR.Location = new System.Drawing.Point(365, 46);
            this.txtSTR.Name = "txtSTR";
            this.txtSTR.Size = new System.Drawing.Size(66, 26);
            this.txtSTR.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(318, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 20);
            this.label11.TabIndex = 9;
            this.label11.Text = "INT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(318, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 20);
            this.label10.TabIndex = 8;
            this.label10.Text = "STR";
            // 
            // txtMDEF
            // 
            this.txtMDEF.Location = new System.Drawing.Point(224, 78);
            this.txtMDEF.Name = "txtMDEF";
            this.txtMDEF.Size = new System.Drawing.Size(66, 26);
            this.txtMDEF.TabIndex = 7;
            // 
            // txtPDEF
            // 
            this.txtPDEF.Location = new System.Drawing.Point(224, 46);
            this.txtPDEF.Name = "txtPDEF";
            this.txtPDEF.Size = new System.Drawing.Size(66, 26);
            this.txtPDEF.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(163, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "MDEF";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(163, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "PDEF";
            // 
            // txtMATK
            // 
            this.txtMATK.Location = new System.Drawing.Point(74, 78);
            this.txtMATK.Name = "txtMATK";
            this.txtMATK.Size = new System.Drawing.Size(66, 26);
            this.txtMATK.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "MATK";
            // 
            // txtPATK
            // 
            this.txtPATK.Location = new System.Drawing.Point(74, 46);
            this.txtPATK.Name = "txtPATK";
            this.txtPATK.Size = new System.Drawing.Size(66, 26);
            this.txtPATK.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "PATK";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1373, 576);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Character Generator";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1373, 576);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Item Database Viewer";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(1269, 534);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(89, 36);
            this.btnAddItem.TabIndex = 5;
            this.btnAddItem.Text = "Save Item";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1395, 627);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "itemMod";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkSetItem;
        private System.Windows.Forms.ComboBox cmbItemSuffix;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbItemPrefix;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbItemQuality;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbItemType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSPD;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtAGI;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDEX;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtINT;
        private System.Windows.Forms.TextBox txtSTR;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMDEF;
        private System.Windows.Forms.TextBox txtPDEF;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMATK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPATK;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRemoveEleProperty;
        private System.Windows.Forms.Button btnAddEleProperty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ListBox listProperties;
        private System.Windows.Forms.ComboBox comboElement;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtEleValue;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkRoundsEnabled;
        private System.Windows.Forms.TextBox txtRounds;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnAddItem;
    }
}

